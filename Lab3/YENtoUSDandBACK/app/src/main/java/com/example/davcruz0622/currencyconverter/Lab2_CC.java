package com.example.davcruz0622.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class Lab2_CC extends AppCompatActivity {
    private EditText editText01;
    private EditText editText02;
    private Button bnt01;
    private TextView textView01;
    private TextView textView02;
    private String usd;
    private String yen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab2__cc);
        editText01 = findViewById(R.id.EditText01);
        editText02 = findViewById(R.id.EditText02);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);
        textView02 = findViewById(R.id.USD);


        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View ConvertToYen) {
                usd = editText01.getText().toString();
                yen = editText02.getText().toString();

                    if(usd.equals(""))
                        {
                            textView01.setText("This field is blank!");
                        }
                     if(yen.equals(""))
                        {
                            textView02.setText("This field is blank!");
                        }
                    else
                    {
                        Double dInputs2 = Double.parseDouble(yen);
                        Double dInputs = Double.parseDouble(usd);


                        Double result2 = dInputs2 / 112.57;
                        Double result = dInputs * 112.57;

                        textView01.setText("$" + usd + " = " + "¥" + String.format("%.2f",result));
                        textView02.setText("¥" + yen + " = " + "$" + String.format("%.2f",result2));
                        editText01.setText("Enter USD ");
                        editText02.setText("Enter Yen");
                    }

            }
        });


    }
}
